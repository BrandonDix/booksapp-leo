from django.contrib import admin
from django.urls import path, include

from booksapplication.views import create_mag_view, delete_mag_view, show_genre, show_magazine, show_magazines, update_mag_view

urlpatterns = [
    path("", show_magazines, name="show_magazines"),
    path('<int:pk>/', show_magazine, name="show_magazine"),
    path('create/', create_mag_view),
    path('<int:pk>/edit/', update_mag_view , name="update_mag_view"),
    path('<int:pk>/delete/', delete_mag_view, name="delete_mag_view"),
    path('genre/', show_genre, name="show_genre")
]