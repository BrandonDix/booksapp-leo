from django.apps import AppConfig


class BooksapplicationConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'booksapplication'
