from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
       return self.name

class Book(models.Model):
    Title = models.CharField(max_length=125, unique=True)
    Author = models.ManyToManyField("Author", related_name="books")
    Details = models.TextField(null=True)
    NumberofPages = models.IntegerField()
    urlofcover = models.URLField(null=True, blank=True)
    ISBN = models.IntegerField()
    Publisheddate = models.IntegerField()
    InPrint = models.BooleanField()

    def __str__(self):
       return self.Title + " by " + str(self.Author.first())

class Magazine(models.Model):
    Title = models.CharField(max_length=125, unique=True)
    Releasecycle = models.TextField(null=True)
    Description = models.TextField(null=True)
    urlofcover = models.URLField(null=True, blank=True)

    def __str__(self):
       return self.Title

class BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField(null=True)


class Issue(models.Model):
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)
    Title = models.CharField(max_length=125, unique=True)
    NumberofPages = models.IntegerField()
    Publisheddate = models.CharField(max_length=20, unique=True)
    IssueNumber = models.IntegerField()
    Description = models.TextField(null=True)
    urlofcover = models.URLField(null=True, blank=True)

class Genre(models.Model):
   name = models.CharField(max_length=125, null=True)
   magazine = models.ManyToManyField(Magazine, related_name="magazines")