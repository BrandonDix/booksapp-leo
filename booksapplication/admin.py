from django.contrib import admin
from booksapplication.models import Author, Book, BookReview, Genre, Issue, Magazine

class BookAdmin(Book):
    pass

admin.site.register(Book)



admin.site.register(Magazine)

admin.site.register(BookReview)
admin.site.register(Author)
admin.site.register(Issue)
admin.site.register(Genre)
