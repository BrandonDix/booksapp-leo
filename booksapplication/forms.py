from dataclasses import fields
from django import forms
from .models import Book, Magazine


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        fields = [
            "Title",
            "Author",
            "Details",
            "NumberofPages",
            "urlofcover",
            "ISBN",
            "Publisheddate",
            "InPrint",
        ]

class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        fields = [
            "Title",
            "Releasecycle",
            "Description",
            "urlofcover",
        ]