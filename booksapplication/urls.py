from django.contrib import admin
from django.urls import path, include
from booksapplication.views import create_view, delete_view, show_book, show_books, update_view

urlpatterns = [
    path("", show_books, name="show_books"),
    path('<int:pk>/', show_book, name="show_book"),
    path('<int:pk>/edit/', update_view, name="update_view"),
    path('<int:pk>/delete/', delete_view, name="delete_view"),
]