from multiprocessing import context
from django.shortcuts import get_object_or_404, redirect, render
from booksapplication.models import Book
from .models import Book, Genre, Magazine
from .forms import BookForm, MagazineForm

# Create your views here.

def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def show_book(request, pk):
    context = {
        "book": Book.objects.get(pk=pk) if Book else None,
    }
    return render(request, "books/details.html", context)


def create_view(request):
    context = {}
    form = BookForm(request.POST or None)
    if form.is_valid():
        book=form.save()
        return redirect("show_book", pk=book.pk)

    context['form']= form
    return render(request, 'books/create_view.html', context)


def update_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book=form.save()
        return redirect("show_book", pk=book.pk)

    context['form']= form
    return render(request, 'books/update_view.html', context)

def delete_view(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("/books")
    return render(request, 'books/delete_view.html', context)

def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines": magazines
    }
    return render(request, "books/maglist.html", context)

def show_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
    }
    return render(request, "books/mag_details.html", context)

def create_mag_view(request):
    context = {}
    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine=form.save()
        return redirect("show_magazine", pk=magazine.pk)

    context['form']= form
    return render(request, 'books/create_mag_view.html', context)

def update_mag_view(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        magazine=form.save()
        return redirect("show_magazine", pk=magazine.pk)

    context['form']= form
    return render(request, 'books/update_mag_view.html', context)

def delete_mag_view(request, pk):
    context = {}
    obj = get_object_or_404(Magazine, pk=pk)
    if request.method == "POST":
        obj.delete()
        return redirect("/magazines")
    return render(request, 'books/delete_mag_view.html', context)

def show_genre(request):
    genre = Genre.objects.all()
    context = {
        "genres" : genre
    }
    return render(request, "books/genre_view.html", context)


